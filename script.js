/* 
Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)

Це така модель документа HTML де кожен тег виступає в ролі обєкту, з яким за допомогує JS можна взаємодіяти - змінювати контент, стилі та розташування.

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

InnerHTML виводить нам текст елементу, розмітку всі теги та їхні атрибути. Може додавати теги до існуючої розмітки
innerText виводить лише текстовий зміст елементу. МОже додавати лише текс до контенту.

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

document.getElementsById
document.getElementsByClassNam
document.getElementsByTagName

querySelector()
querySelectorAll()

Складно сказати який краще. Все залежить від поставленої задачі. Але частіше викоростовують querySelector()


4. Яка різниця між nodeList та HTMLCollection?

NodeList повертає нам всі вузли дерева DOM. НЕ тільки позмітку, а й коментарі, різні приховані тексти, табуляції та переноси рядків.

HTMLCollection повернтає нам колекцію елементи DOM дерева. Тобто всю HTML розміку (але без коментарів, прихованих текстів і символів).

Практичні завдання
 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
 Використайте 2 способи для пошуку елементів. 
 Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).
 
 2. Змініть текст усіх елементів h2 на "Awesome feature".

 3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".
 */

const feature1 = document.getElementsByClassName('feature')
const feature2 = document.querySelectorAll('.feature')
console.log(feature1)
console.log(feature2)

const feature = [...document.getElementsByClassName('feature')]
feature.forEach((el) => {
  el.style.textAlign = 'center'
})

const title = [...document.getElementsByTagName('h2')]
title.forEach((el) => {
  el.textContent = 'Awesome feature'
})

const featureTitle = [...document.getElementsByClassName('feature-title')]
featureTitle.forEach((el) => {
  el.textContent += '!'
})
